<?php
$pageTitle = "Welcome";

include './inc/head.php';
include './inc/navbar.php';

?>
<script type="text/javascript">
<!--
  function toggle_visibility(id) {
var e = document.getElementById(id);
e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}
//-->
</script>
<div class="top">
	<div class="inner left">
		<h1>Quality <br> Attachments</h1>
		<h5>for skid steers, telehandlers, tractors and others</h5>

		<a href="./products">
			<button type="button" class="btn"> Browse Our Products </button>
		</a>
		<a href="./contact">
			<button type="button" class="btn btn-top">Enquire</button>
		</a>


	</div>

</div>

<article class="frontpage">


<section>
	<div class="light">
		<div class="inner">
			<div class="front-text">
				<h4>Attachments for a wide variety of applications and vehicles</h4>
				<p>Our attachments are universal, and designed to natively fit the vast majority of applications. Manufacturers include: Bobcat, John Deere, JCB, Case, Caterpillar, Daewoo, New Holland, Terex and Volvo.</p>
				<p>Even if your vehicle is not included in this list, it is likely that we do support it. Please quote the manufacturer and model when contacting for a quote, as older models may have insufficient hydraulic power. Whatever your circumstances, we will do our utmost to accommodate your requirements.</p>
				<a href="./contact">
					<button type="button" class="btn">Contact Us</button>
				</a>
			</div>
		<img class="front-img" src="<?php echo $baseURL; ?>/img/collage1.jpg" alt="wide variety of attachments">
		</div>
	</div>
	<div class="dark">
		<div class="inner">
			<div class="front-text">
				<h4>Quality machinery that will work above the norm</h4>
				<p>We use the latest technology in our machinery, such as Hardox Steel to increase the flexibility and durability of our machinery in the most testing and harshest environments. Not only are our machines durable and hardwearing but they are also modular during the purchase and can be adapted to suit your requirements in the field.</p>
				<a href="./products">
					<button type="button" class="btn">Browse Our Products</button>
				</a>
			</div>
				<img class="front-img" src="<?php echo $baseURL; ?>/img/snow-blower.jpg" alt="Snow Blower">
		</div>
	</div>
	<div class="light">
		<div class="inner">
			<div class="front-text">
				<h4>We've been in business since 1996</h4>
				<p>
					We've been a limited family company for over fifteen years in the United Kingdom, so we'll always be able to support you and your business requirements. We take pride in our ability to equip you with the tools to tackle the toughest challenges, while at the same time delivering at an affordable but reasonable price. 
				</p>
					<a href="./aboutus"><button type="button" class="btn">About Us</button>
				</a>
			</div>
				<img class="front-img" src="<?php echo $baseURL; ?>/products/img/auger9.jpg" alt="Established for twenty years">
		</div>
	</div>
</section>
</article>
<?php include './inc/footer.php'; ?>
