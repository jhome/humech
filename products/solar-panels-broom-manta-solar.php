<?php $productUrl = "solar-panels-broom-manta-solar"; ?>
<?php $productTitle = "Solar Panels Broom Manta Solar"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Broom specifically designed for solar panel cleaning, provided with <em>Carlite</em>® brushes, a patented anti-scratch material, wear resistant and certified waterproof properties.</p>
<p>The Solar Panel Broom Manta Solar is complemented with rubber hoses for hydraulic circuit implementation; sprinkler kit consisting of two bars for washing, and two bars for surfaces rinsing and sprinkler nozzle; digital position sensor kit complete with lights and sound signals box.</p>
<p>Removable sides supports for equipment storage.</p>
<ul>
	<li>1000 lt. tank + forks kit to lift the tank + double electric pump, hoses and electrical cables.</li>
	<li>Plant for water treatment by 500 litres per day of yeld.</li>
	<li>Plant for water treatment by 1500 litres per day of yeld.</li>
	<li>Plant for water treatment by 3000 litres per day of yeld.</li>
	<li>Escavators bolted plate.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>Manta Solar 2100 L</th>
			<th>Manta Solar 2100</th>
			<th>Manta Solar 3500</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Working width (mm.)</td>
			<td>1850</td>
			<td>1850</td>
			<td>3360</td>
		</tr>
		<tr>
			<td>Total width (mm.)</td>
			<td>2100</td>
			<td>2100</td>
			<td>3500</td>
		</tr>
		<tr>
			<td>Height (mm.)</td>
			<td>1300</td>
			<td>1300</td>
			<td>1300</td>
		</tr>
		<tr>
			<td>Weight without linkage (kg.)</td>
			<td>135</td>
			<td>330</td>
			<td>460</td>
		</tr>
		<tr>
			<td>Brushes (Ø mm./nr.)</td>
			<td>1000/23</td>
			<td>1000/23</td>
			<td>1000/42</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>