<!-- type in the product title and url -->
<?php $productUrl = "mixer-bucket-condor-sl"; ?>
<?php $productTitle = "Mixer Bucket Condor SL"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Side discharge beton mixer bucket equipped by screw shaft in Hardox metal, electro-valve with button for hydraulic unloading plate opening, unloading cylinder with protected rod, bolted protection grate, rubber pipes for hydraulic circuit supply, universal linkage kit for skid-steer loader.</p>
<ul>
	<li>Pressure regulation valve</li>
	<li>Hydraulic block of rotation’ shaft</li>
	<li>High pressure and high oil flow motor</li>
	<li>Gas spring complete by hydraulic block of rotation’ shaft</li>
	<li>Bolted plate for interchangeable attachments</li>
	<li>Bolted plate for excavators</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Connector with 8 or 14 poles</li>
	<li>Moldboard in Hardox metal</li>
	<li>Reel</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>CONDOR 250 SL</th>
			<th>CONDOR 300 SL</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Screw shaft diameter (mm.)</td>
			<td>480</td>
			<td>480</td>
		</tr>
		<tr>
			<td>Total bucket width (mm.)</td>
			<td>1490</td>
			<td>1575</td>
		</tr>
		<tr>
			<td>Working bucket width (mm.)</td>
			<td>1260</td>
			<td>1345</td>
		</tr>
		<tr>
			<td>Empty weight (kg.)</td>
			<td>340</td>
			<td>355</td>
		</tr>
		<tr>
			<td>Total weight at maximum capacity (kg.)</td>
			<td>1060</td>
			<td>1140</td>
		</tr>
		<tr>
			<td>Maximum capacity (lt.)</td>
			<td>300</td>
			<td>325</td>
		</tr>
		<tr>
			<td>Working capacity (lt.)</td>
			<td>260</td>
			<td>305</td>
		</tr>
		<tr>
			<td>Maximum oil flow (l/1')</td>
			<td>90</td>
			<td>90</td>
		</tr>
		<tr>
			<td>Maximum pressure (bar)</td>
			<td>215</td>
			<td>215</td>
		</tr>
		<tr>
			<td>Unloading hole diameter (mm.)</td>
			<td>180</td>
			<td>180</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>