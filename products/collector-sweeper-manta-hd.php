<?php $productUrl = "collector-sweeper-manta-hd"; ?>
<?php $productTitle = "Collector Sweeper Manta HD"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>


<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6','7','8','9','10'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Collector sweeper with polypropylene brushes (switchable with steel brushes), universal linkage kit for Skid-Steer Loader, front and side protections with rubber bands.</p>
<p>Actioned by the hydraulic circuit of the machinery.</p>
<p>Wear resistant double cut bolted blade.</p>

<ul>
	<li>Pressure water kit with 100 litres tank, electric pump and sprinklers..</li>
	<li>Pressure water kit with tank present on the machinery</li>
	<li>Gravity water kit with 100 litres tank.</li>
	<li>Side brush Ø 580mm with indipendent hydraulic engine.</li>
	<li>Pipe kit with sprays for side brush</li>
	<li>High pressure and high oil flow motor</li>
	<li>Connector with 8 or 14 poles</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Pressure regulation valve</li>
	<li>Pressure and oil flow regulation valve</li>
	<li>Sliding steel bucket’s wheels (nr.4)</li>
	<li>Floating connection plate</li>
	<li>Excavator bolted plate</li>
	<li>Double linkage kit: excavator bolted plate and SSL universal linkage</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>Manta 1400 HD</th>
			<th>Manta 1550 HD</th>
			<th>Manta 1680 HD</th>
			<th>Manta 1800 HD</th>
			<th>Manta 2100 HD</th>
			<th>Manta 2500 HD</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1520</td>
			<td>1670</td>
			<td>1800</td>
			<td>1920</td>
			<td>2220</td>
			<td>2620</td>
		</tr>
		<tr>
			<td>Working width (mm.)</td>
			<td>1400</td>
			<td>1550</td>
			<td>1680</td>
			<td>1800</td>
			<td>2100</td>
			<td>2500</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>355</td>
			<td>400</td>
			<td>425</td>
			<td>450</td>
			<td>485</td>
			<td>610</td>
		</tr>
		<tr>
			<td>Brush (mm/nr.)</td>
			<td>560/30</td>
			<td>560/34</td>
			<td>560/36</td>
			<td>560/38</td>
			<td>560/48</td>
			<td>560/52</td>
		</tr>
		<tr>
			<td>Bucket capacity (lt.)</td>
			<td>400</td>
			<td>440</td>
			<td>475</td>
			<td>510</td>
			<td>596</td>
			<td>706</td>
		</tr>
		<tr>
			<td>Maximum oil flow (l/1')</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>100</td>
		</tr>
		<tr>
			<td>Maximum capacity (bar.)</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>245</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>