<?php $productUrl = "tilt-dozer-blade"; ?>
<?php $productTitle = "Tilt Dozer Blade"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Tilt dozer blade or high tilt dozer blade with double acting cylinders for left-right hydraulic rotation, rubber pipes and quick couplers for the hydraulic circuit supply and universal linkage plate.</p>
<p>Laser support plate included on LTDA models.</p>
<ul>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Laser support plate (included in LTDA)</li>
	<li>Connector with 8 or 14 poles</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>LTD 1800</th>
			<th>LTD 2100</th>
			<th>LTD 2400</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Blade width (mm.)</td>
			<td>1800</td>
			<td>2100</td>
			<td>2400</td>
		</tr>
		<tr>
			<td>Working width with angled blade (mm.)</td>
			<td>1558</td>
			<td>1818</td>
			<td>2078</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>340</td>
			<td>380</td>
			<td>420</td>
		</tr>
		<tr>
			<td>Oscillation</td>
			<td>±15°</td>
			<td>±15°</td>
			<td>±15°</td>
		</tr>
		<tr>
			<td>Angulation</td>
			<td>±30°</td>
			<td>±30°</td>
			<td>±30°</td>
		</tr>
		<tr>
			<td>Moldboard high (mm.)</td>
			<td>540</td>
			<td>540</td>
			<td>540</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>