<?php $productUrl = "collector-sweeper-manta-i"; ?>
<?php $productTitle = "Collector Sweeper Manta I"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product"><ul class="img-list">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6','7','8'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Collector sweeper with polypropylene brushes (switchable with steel brushes) installed on a floating rotor, 3 or 4 slewing wheels 360° frees, hydraulically actuated cylinder for waste dumping, rubber pipes and quick couplers for hydraulic circuit supply.</p>
<ul>
	<li>Pressure water kit with 100 litres tank, electric pump and sprinklers.</li>
	<li>Pressure water kit with tank present on the machinery</li>
	<li>Gravity water kit with 100 litres tank</li>
	<li>Side brush Ø 580mm with independent hydraulic engine</li>
	<li>Pipe kit with sprays for side brush</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Connector with 8 or 14 poles</li>
	<li>High pressure and high oil flow motor</li>
	<li>Pressure regulation valve</li>
	<li>Pressure and oil flow regulation valve</li>
	<li>Floating connection system for agricultural tractors</li>
	<li>Floating connection system for skid-steer loaders</li>
	<li>Floating connection system for loaders, backhoe loaders and telehandlers</li>
	<li>Floating connection system for pallet fork machinery</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>Manta 1200 I</th>
			<th>Manta 1400 I</th>
			<th>Manta 1600 I</th>
			<th>Manta 1800 I</th>
			<th>Manta 2100 I</th>
			<th>Manta 2400 I</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1400</td>
			<td>1600</td>
			<td>1800</td>
			<td>2000</td>
			<td>2300</td>
			<td>2600</td>
		</tr>
		<tr>
			<td>Working width(mm.)</td>
			<td>1200</td>
			<td>1400</td>
			<td>1600</td>
			<td>1800</td>
			<td>2100</td>
			<td>2400</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>400</td>
			<td>430</td>
			<td>470</td>
			<td>530</td>
			<td>590</td>
			<td>640</td>
		</tr>
		<tr>
			<td>Brush (mm/nr.)</td>
			<td>560/28</td>
			<td>560/30</td>
			<td>560/36</td>
			<td>560/38</td>
			<td>560/45</td>
			<td>560/52</td>
		</tr>
		<tr>
			<td>Bucket capacity (lt.)</td>
			<td> 180</td>
			<td>230</td>
			<td>290</td>
			<td>350</td>
			<td>410</td>
			<td>470</td>
		</tr>
		<tr>
			<td>Maximum oil flow (l/1')</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>100</td>
		</tr>
		<tr>
			<td>Maximum Pressure (bar.)</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>245</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>