<?php $productUrl = "snow-blower-yeti-std"; ?>
<?php $productTitle = "Snow Blower Yeti STD"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6','7','8','9'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Double stage frontal snow blower working through the vehicle hydraulic plant. It is complete with bolted blade in wear resistant steel; side adjustable slides; cutting edge helixes for ice, mechanical adjustable edge chute and flaps. It can be equipped with polyurethane blades to avoid surface damage. Each application is realised by equipping the snow blower with different size of hydraulic motor to increase the rotation speed.</p>
<ul>
    <li>Hydraulic chute rotation and flap orientation.</li>
    <li>Quick couplers kit 3/4 S.F.</li>
    <li>Connector with 8 or 14 poles .</li>
    <li>Polyurethane blade replacing the original one.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>YETI 1600 STD</th>
			<th>YETI 1800 STD</th>
			<th>YETI 2100 STD</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Working width (mm.)</td>
			<td>1600</td>
			<td>1800</td>
			<td>2100</td>
		</tr>
		<tr>
			<td>1st and 2nd stage roller diameter (mm.)</td>
			<td>450/420</td>
			<td>450/420</td>
			<td>450/420</td>
		</tr>
		<tr>
			<td>Weight (Kg.)</td>
			<td>420</td>
			<td>480</td>
			<td>540</td>
		</tr>
		<tr>
			<td>Frontal height (mm.)</td>
			<td>740</td>
			<td>740</td>
			<td>740</td>
		</tr>
		<tr>
			<td>Rotation edge chute (variant set up)</td>
			<td>240°</td>
			<td>240°</td>
			<td>240°</td>
		</tr>
		<tr>
			<td>Working pressure (bar)</td>
			<td>140-170</td>
			<td>140-170</td>
			<td>140-170</td>
		</tr>
		<tr>
			<td>Maximum pressure (bar)</td>
			<td>210</td>
			<td>210</td>
			<td>210</td>
		</tr>
		<tr>
			<td>Continious-intermittent oil flow (l/1')</td>
			<td>45-65/70-120</td>
			<td>45-65/70-120</td>
			<td>45-65/70-120</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>