<!-- type in the product title and url -->
<?php $productUrl = "tilt-tatch"; ?>
<?php $productTitle = "Tilt Tatch"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>
<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6','7','8'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>The Tilt-Tatch connection system is an interface located between the Skid-Steer Loader linkage and the attachment. Thanks to its hydraulic right/left oscillation, the Tilt-Tatch is the ideal tool to fix charge’s level on rough grounds or for many other operations such as drainage ditches digging with the bucket corner.</p>
<ul>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Electric valve kit for snowblade or angledozer blade coupling.</li>
	<li>Connector with 8 or 14 poles.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>TILT-TATCH</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1240</td>
		</tr>
		<tr>
			<td>Length (mm.)</td>
			<td>181</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>140</td>
		</tr>
		<tr>
			<td>Height (mm.)</td>
			<td>720</td>
		</tr>
		<tr>
			<td>Oscillation</td>
			<td>±16°</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>