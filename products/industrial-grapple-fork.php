<!-- type in the product title and url -->
<?php $productUrl = "industrial-grapple-fork"; ?>
<?php $productTitle = "Industrial Grapple Fork"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Industrial grapple fork with double grip powered by double acting hydraulic cylinders. It is equipped as standard with a blocking valve; universal linkage plate; rubber pipes and quick couplers for hydraulic circuit supply.</p>
<ul>
	<li>Side kit for increased bucket capacity</li>
	<li>Quick couplers kit 3/4 S.F.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>FPRI 1400</th>
			<th>FPRI 1550</th>
			<th>FPRI 1680</th>
			<th>FPRI 1800</th>
			<th>FPRI 2030</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1420</td>
			<td>1570</td>
			<td>1700</td>
			<td>1820</td>
			<td>2030</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>320</td>
			<td>340</td>
			<td>350</td>
			<td>360</td>
			<td>410</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>