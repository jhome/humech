<!-- type in the product title and url -->
<?php $productUrl = "industrial-backhoe"; ?>
<?php $productTitle = "Industrial Backhoe"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Hydraulic backhoe with translatable frame and mechanical stops. Independent, hydraulically actuated stabiliser feet with main pin joints in hardened and heat treated steel (except for E15 and E19 model). Linkage kit for Skid-Steer Loader and Backhoe Loader.</p>
<ul>
	<li>Frame translation hydraulic block (not applicable for E15 model) </li>
	<li>Breaker hammer working kit.</li>
	<li>Extendable arm.</li>
	<li>Cloche control valve for E21-E24 models (included for model E25)</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>E21</th>
			<th>E24</th>
			<th>E25</th>
			<th>E28</th>
			<th>E30</th>
			<th>E32</th>
			<th>E35</th>
			<th>E37</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Digging depth (mm.)</td>
			<td>2180</td>
			<td>2480</td>
			<td>2580</td>
			<td>2780</td>
			<td>3150</td>
			<td>3260</td>
			<td>3350</td>
			<td>3570</td>
		</tr>
		<tr>
			<td>Overponed hydraulic cylinders</td>
			<td>180°</td>
			<td>180°</td>
			<td>180°</td>
			<td>180°</td>
			<td>180°</td>
			<td>180°</td>
			<td>180°</td>
			<td>180°</td>
		</tr>
		<tr>
			<td>Frame largeness (mm.)</td>
			<td>1200</td>
			<td>1200</td>
			<td>1400</td>
			<td>1600</td>
			<td>1800</td>
			<td>1800</td>
			<td>1800</td>
			<td>1800</td>
		</tr>
		<tr>
			<td>Pressure (bar)</td>
			<td>160</td>
			<td>160</td>
			<td>170</td>
			<td>170</td>
			<td>170</td>
			<td>170</td>
			<td>170</td>
			<td>170</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>450</td>
			<td>520</td>
			<td>620</td>
			<td>750</td>
			<td>980</td>
			<td>1020</td>
			<td>1140</td>
			<td>1200</td>
		</tr>
		<tr>
			<td>Extension</td>
			<td></td>
			<td></td>
			<td>•</td>
			<td>•</td>
			<td>•</td>
			<td>•</td>
			<td>•</td>
			<td>•</td>
		</tr>
		<tr>
			<td>Oil flow (l/1')</td>
			<td>55</td>
			<td>55</td>
			<td>60</td>
			<td>60</td>
			<td>80</td>
			<td>80</td>
			<td>80</td>
			<td>80</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>