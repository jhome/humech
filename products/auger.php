<?php $productUrl = "auger"; ?>
<?php $productTitle = "Auger"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6','7','8','9','10','11','12'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Head attachment with motor driver and universal joint included, exit shaft located at clutch for insertion of drills. Available continuous flight auger drills:</p>
<ul>
	<li>Continuous flight auger drills for work on soil - double helics and bolted antiwear steel edges</li>
	<li>Continuous flight auger drills for work on stone ground -  double helics, <em>PENGO</em> point and tungsten teeth</li>
	<li>Continuous flight auger drills for for work on asphalt and rocky ground -  double helics thickness 20 mm, tungsten boring teeth and special drill.</li>
</ul>

<p>Drills are available with diameters from 150 mm to 800 mm. and length from 1000 mm to 1500 mm. Extension drills with/without helices are available with length from 500 mm to 1500 mm.</p>

<ul>
	<li>Locking head for horizontal holes</li>
	<li>Electric reversal with reversing rotation by electrical control for simplex hydraulic plant</li>
	<li>Universal and non universal plate for Skid-Steer Loader</li>
	<li>Linkage plate for Loaders, Backhoe Loaders and Telehandlers</li>
	<li>Extension drills with/without helics</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>TR 05</th>
			<th>TR 10</th>
			<th>TR 20</th>
			<th>TR 45</th>
			<th>TR 65</th>
			<th>TR 90</th>
			<th>TR 150</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Motor (cc.)</td>
			<td>50</td>
			<td>100</td>
			<td>160</td>
			<td>160</td>
			<td>250</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Motor axial cylinders</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>H1C55</td>
			<td>H1C75</td>
		</tr>
		<tr>
			<td>Oil flow (l/1')</td>
			<td>15÷40</td>
			<td>15÷45</td>
			<td>25÷65</td>
			<td>25÷70</td>
			<td>20÷125</td>
			<td>40÷140</td>
			<td>60÷200</td>
		</tr>
		<tr>
			<td>Pressure (bar)</td>
			<td>130</td>
			<td>165</td>
			<td>210</td>
			<td>210</td>
			<td>240</td>
			<td>350</td>
			<td>350</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>40</td>
			<td>60</td>
			<td>90</td>
			<td>110</td>
			<td>265</td>
			<td>290</td>
			<td>350</td>
		</tr>
		<tr>
			<td>Max drill (mm.)</td>
			<td>400</td>
			<td>400</td>
			<td>600</td>
			<td>600</td>
			<td>800</td>
			<td>1000</td>
			<td>1000</td>
		</tr>
		<tr>
			<td>Hexagon (Ø)</td>
			<td>50</td>
			<td>50</td>
			<td>50</td>
			<td>50</td>
			<td>70</td>
			<td>70</td>
			<td>70</td>
		</tr>
		<tr>
			<td>Max torque (DaNm)</td>
			<td>104</td>
			<td>126</td>
			<td>207</td>
			<td>359</td>
			<td>1061</td>
			<td>1300</td>
			<td>1410</td>
		</tr>
		<tr>
			<td>Axial load (DaN)</td>
			<td>900</td>
			<td>900</td>
			<td>3500</td>
			<td>3500</td>
			<td>5000</td>
			<td>6000</td>
			<td>6800</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>