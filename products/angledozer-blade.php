<?php $productUrl = "angledozer-blade"; ?>
<?php $productTitle = "Angledozer Blade"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1'); ?>
<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Blade for dozing works with left-right angulation attachment to either an hydraulic support with connection device for Skid-Steer Loader; agricultural tractors; or forklift trucks. It is equipped as standard with rubber hoses for hydraulic circuit supply.</p>
<ul>
    <li>Valve block for central neutral position</li>
    <li>Marker flags and lamp kit complete with supports</li>
    <li>Quick couplers kit 3/4 S.F.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>LA-1500</th>
			<th>LA-1600</th>
			<th>LA-1800</th>
			<th>LA-2100</th>
			<th>LA-2400</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Blade width (mm.)</td>
			<td>1500</td>
			<td>1600</td>
			<td>1800</td>
			<td>2100</td>
			<td>2400</td>
		</tr>
		<tr>
			<td>Working width with angled blade ( mm.)</td>
			<td>1300</td>
			<td>1385</td>
			<td>1558</td>
			<td>1818</td>
			<td>2078</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td><em></em></td>
			<td>250</td>
			<td>265</td>
			<td>290</td>
			<td>315</td>
		</tr>
		<tr>
			<td>Angulation (°)</td>
			<td>±30°</td>
			<td>±30°</td>
			<td>±30°</td>
			<td>±30°</td>
			<td>±30°</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>