<?php $productUrl = "bale-fork"; ?>
<?php $productTitle = "Bale Fork"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Model FRP</p>
<p>FEM (rules for the design of lifting devices) bales fork with anti-overturning protection, two translatable forks and universal linkage kit for Skid-Steer Loaders.</p>
<p>Model FR</p>
<p>FEM (rules for the design of lifting devices) bale fork without anti-overturning protection, two translatable forks and linkage kit for Wheel, Blackhoe Loaders and Telehandlers.</p>
<ul>
	<li>Pallet teeth</li>
    <li>Anti-overturning protection for FR models</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>FRP 10-12</th>
			<th>FRP 10-14</th>
			<th>FRP 16-15</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Fork (Ø)</td>
			<td>42</td>
			<td>42</td>
			<td>42</td>
		</tr>
		<tr>
			<td>Width (D mm.)</td>
			<td>1210</td>
			<td>1410</td>
			<td>1510</td>
		</tr>
		<tr>
			<td>Capacity (kg.)</td>
			<td>1000</td>
			<td>1000</td>
			<td>1600</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>130</td>
			<td>155</td>
			<td>205</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>