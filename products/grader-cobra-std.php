<!-- type in the product title and url -->
<?php $productUrl = "grader-cobra-std"; ?>
<?php $productTitle = "Grader Cobra STD"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Grader with hydraulic blade translation, ± 35° rotation, ± 27° rotation and fitting plate kit. It is possible to provide the grader with a proportional or on/off electric control valve.</p>
<p>Controls include: joystick, 14 pins connector, manipulator with 3 levers or radio control.</p>
<ul>
	<li>4 teeth ripper with mechanical position</li>
	<li>On/off controvalve with joystick.</li>
	<li>On/off controvalve with radio remote control</li>
	<li>On/off controvalve with 3 levers manipulator</li>
	<li>On/off controvalve with 14 poles connector</li>
	<li>Proportional controvalve with joystick</li>
	<li>Proportional controvalve with radio remote control</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Laser support plate</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>COBRA 2100STD</th>
			<th>COBRA 2400STD</th>
			<th>COBRA 2500STD</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Blade width (mm.)</td>
			<td>2100</td>
			<td>2400</td>
			<td>2500</td>
		</tr>
		<tr>
			<td>
</td>
			<td>1780</td>
			<td>2025</td>
			<td>2105</td>
		</tr>
		<tr>
			<td>Side translation to right-left to centre (mm.)</td>
			<td><em>±</em></td>
			<td><em>±</em></td>
			<td><em>±</em></td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>630</td>
			<td>660</td>
			<td>670</td>
		</tr>
		<tr>
			<td>Oil flow (l/1')</td>
			<td>80</td>
			<td>80</td>
			<td>80</td>
		</tr>
		<tr>
			<td>Pressure (bar)</td>
			<td>150</td>
			<td>150</td>
			<td>150</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>