<?php $productUrl = "crusher-bucket"; ?>
<?php $productTitle = "Crusher Bucket"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>The crusher bucket is the ideal solution in any working environment such as quarries and pits due to the immediate on-site crushing of inert materials and reuse of waste material on site. It works especially well in medium or small work sites due to easy handling.</p>
<p>Hardox Steel is utilised resulting in minimum wear and increased crush resistance. </p>
<p>For use on a excavator. </p>
<ul>
<h6>Accessories</h6>
	<li>Front excavation teeth</li>
	<li>Back excavation teeth</li>
	<li>Magnet kit + control panel</li>
	<li>Inged lodestone</li>
	<li>Pre-arrangement for wheel loader or telescopic</li>
</ul>
<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>CBF 05</th>
			<th>CBF 06</th>
			<th>CBF 07</th>
			<th>CBF 09</th>
			<th>CBF 12</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Evavator weight (ton)</td>
			<td>7 - 10</td>
			<td>10 - 13</td>
			<td>13 - 20</td>
			<td>20 - 30</td>
			<td>30 - 50</td>

		</tr>
		<tr>
			<td>Bucket size (mm)</td>
			<td>500x420</td>
			<td>600x500</td>
			<td>700x550</td>
			<td>900x550</td>
			<td>1200x550</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>1250</td>
			<td>1630</td>
			<td>2250</td>
			<td>3500</td>
			<td>5000</td>
		</tr>
		<tr>
		<td>SAE capability(m³)</td>
			<td>0,43</td>
			<td>0,58</td>
			<td>0,7</td>
			<td>0,8</td>
			<td>1</td>
		</tr>
		<tr>
		<td>Maximum pressure (bar)</td>
			<td>200</td>
			<td>200</td>
			<td>200</td>
			<td>200</td>
			<td>200</td>
		</tr>
		<tr>
		<td>Flow(l/min)</td>
			<td>120</td>
			<td>130</td>
			<td>150</td>
			<td>180</td>
			<td>220</td>
		</tr>
		<tr>
		<td>Opening</td>
			<td>25/100</td>
			<td>25/100</td>
			<td>25/125</td>
			<td>25/125</td>
			<td>25/125</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>