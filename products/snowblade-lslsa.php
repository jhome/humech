<?php $productUrl = "snowblade"; ?>
<?php $productTitle = "Self-levelling Snowblade"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Snowblade and high snowblade with horizontal self levelling, anti-shock system with clip return; fixing rods for trenching work; hydraulic rotation left-right with double acting cylinders; rubber pipes and quick couplers for the hydraulic circuit supply; marker flags and lamp kit complete with light supports and universal linkage plate.</p>
<p>Adjustable feet included in LSA models, and on request with LS 1800 and LS 2100 </p>
<ul>
	<li>Blocking valve</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Connector with 8 or 14 poles</li>
	<li>Top strip for high speed (LSA only).</li>
	<li>Polyurethane blade in addition to the original</li>
	<li>Hydraulic lift for agricultural tractors.</li>
	<li>Adjustable feet (for LS1800 and lS2100 models only).</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>LS 1500</th>
			<th>LS 1650</th>
			<th>LS 1800</th>
			<th>LS 2100</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Blade width (mm.)</td>
			<td>1500</td>
			<td>1650</td>
			<td>1800</td>
			<td>2100</td>
		</tr>
		<tr>
			<td>Working width with angled blade (mm.)</td>
			<td>1300</td>
			<td>1400</td>
			<td>1558</td>
			<td>1818</td>
		</tr>
		<tr>
			<td>Moldboard highness (mm.)</td>
			<td>700</td>
			<td>700</td>
			<td>700</td>
			<td>700</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td><em></em></td>
			<td>255</td>
			<td>280</td>
			<td>315</td>
		</tr>
		<tr>
			<td>Angulation</td>
			<td>±30°</td>
			<td>±30°</td>
			<td>±30°</td>
			<td>±30°</td>
		</tr>
		<tr>
			<td>Self-levelling</td>
			<td>±10°</td>
			<td>±10°</td>
			<td>±10°</td>
			<td>±10°</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>