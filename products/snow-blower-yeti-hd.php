<?php $productUrl = "snow-blower-yeti-hd"; ?>
<?php $productTitle = "Snow Blower Yeti HD"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Double stage frontal snow blower working through the vehicle hydraulic plant. It is complete with bolted blade in wear resistant steel; side adjustable slides; cutting edge helixes for ice, mechanical adjustable edge chute and flaps. It can be equipped with polyurethane blades to avoid surface damage. Each application is realised by equipping the snow blower with different size of hydraulic motor to increase the rotation speed.</p>
<ul>
	<li>Hydraulic chute rotation and flap orientation</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Connector with 8 or 14 poles.</li>
	<li>Polyurethane blade replacing the original</li>
</ul>

<!-- table -->
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>YETI 2100 HD</th>
			<th>YETI 2300 HD</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Working width (mm.)</td>
			<td>2100</td>
			<td>2300</td>
		</tr>
		<tr>
			<td>1st and 2nd stage roller diameter (mm.)</td>
			<td>630/550</td>
			<td>630/550</td>
		</tr>
		<tr>
			<td>Weight (Kg.)</td>
			<td>890</td>
			<td>950</td>
		</tr>
		<tr>
			<td>Frontal height (mm.)</td>
			<td>1000</td>
			<td>1000</td>
		</tr>
		<tr>
			<td>Rotation edge chute (variant set up)</td>
			<td>240°</td>
			<td>240°</td>
		</tr>
		<tr>
			<td>Working pressure (bar)</td>
			<td>140-170</td>
			<td>140-170</td>
		</tr>
		<tr>
			<td>Continious-intermittent pressure (bar)</td>
			<td>220-250</td>
			<td>220-250</td>
		</tr>
		<tr>
			<td>Continious-intermittent oil flow (l/1')</td>
			<td>90-140</td>
			<td>90-140</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>