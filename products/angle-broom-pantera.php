<?php $productUrl = "angle-broom-pantera"; ?>
<?php $productTitle = "Angle Broom Pantera"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Angle broom “Pantera” does not have a collector bucket but includes polypropylene brushes (switchable with steel brushes), mechanical angulation, rubber hoses for hydraulic circuit supply, universal linkage plate for Skid-Steer Loader.</p>
<ul>
	<li>Broom rotation by electrically operated valve and double acting cylinders; supplied with electrical fittings </li>
	<li>Gravity water kit with 100 litres tank</li>
	<li>Pressure water kit available with or without 100 litres tank, electric pump and sprinklers.</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Blocking valve</li>
	<li>High pressure and high oil flow motor</li>
	<li>Pressure regulation valve</li>
	<li>Pressure and oil flow regulation valve</li>
	<li>Connector with 8 or 14 poles.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>Pantera 1250</th>
			<th>Pantera 1550</th>
			<th>Pantera 1800</th>
			<th>Pantera 2100</th>
			<th>Pantera 2400</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1350</td>
			<td>1650</td>
			<td>1900</td>
			<td>2200</td>
			<td>2500</td>
		</tr>
		<tr>
			<td>Working width (mm.)</td>
			<td>1250</td>
			<td>1550</td>
			<td>1800</td>
			<td>2100</td>
			<td>2400</td>
		</tr>
		<tr>
			<td>Total angled width (mm.)</td>
			<td>1413</td>
			<td>1725</td>
			<td>1925</td>
			<td>2185</td>
			<td>2445</td>
		</tr>
		<tr>
			<td>Angled working width (mm.)</td>
			<td>1055</td>
			<td>1308</td>
			<td>1489</td>
			<td>1741</td>
			<td>2002</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>300</td>
			<td>340</td>
			<td>370</td>
			<td>410</td>
			<td>450</td>
		</tr>
		<tr>
			<td>Brush (mm/nr.)</td>
			<td>560/28</td>
			<td>560/34</td>
			<td>560/38</td>
			<td>560/45</td>
			<td>560/52</td>
		</tr>
		<tr>
			<td>Angulation</td>
			<td> ±30°</td>
			<td>±30°</td>
			<td>±30°</td>
			<td> ±30°</td>
			<td>±30°</td>
		</tr>
		<tr>
			<td>Maximum oil flow (l/1')</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>100</td>
		</tr>
		<tr>
			<td>Maximum presure (bar.)</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>245</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>