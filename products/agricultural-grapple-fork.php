<?php $productUrl = "agricultural-grapple-fork"; ?>
<?php $productTitle = "Agricultural Grapple Fork"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Agricultural grapple fork with double grip driven by double acting hydraulic cylinders.</p>
<p>It is equipped as standard with blocking valve; rubber pipes and quick couplers for hydraulic circuit supply; bolted teeth with removable protection and universal linkage plate.</p>
<ul>
	<li>Quick couplers kit 3/4 S.F.</li>
</ul>
<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>FPRA 1400</th>
			<th>FPRA 1550</th>
			<th>FPRA 1600</th>
			<th>FPRA 1800</th>
			<th>FPRA 2030</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1420</td>
			<td>1570</td>
			<td>1700</td>
			<td>1820</td>
			<td>2030</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>265</td>
			<td>297</td>
			<td>313</td>
			<td>330</td>
			<td>365</td>
		</tr>
		<tr>
			<td>Tooth lengh (mm.)</td>
			<td>700</td>
			<td>700</td>
			<td>700</td>
			<td>700</td>
			<td>700</td>
		</tr>
		<tr>
			<td>Teeth (nr.)</td>
			<td>9</td>
			<td>10</td>
			<td>11</td>
			<td>11</td>
			<td>12</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>