<?php $productUrl = "agricultural-fork"; ?>
<?php $productTitle = "Agricultural Fork"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>
<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<!-- No iamges for this product -->
<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		if (isset($img)){
			foreach($img as $img) {
				if ($img++ == 1) break;
				echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
			}
		}
	?>
</ul>

<!-- pdf  -->
<!-- Not pdf -->

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Agricultural Fork with bolted teeth and universal linkage plate. It is equipped as standard with teeth removable protection.</p>
<ul>
	<li>Agricultural grapple fork kit comprising of grip, double effect hydraulic cylinder, rubber pipes and quick couplers.</li>
</ul>
<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>FA 1400</th>
			<th>FA 1550</th>
			<th>FA 1680</th>
			<th>FA 1800</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width(mm.)</td>
			<td>1400</td>
			<td>1550</td>
			<td>1680</td>
			<td>1800</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>145</td>
			<td>165</td>
			<td>180</td>
			<td>195</td>
		</tr>
		<tr>
			<td>Tooth lenght (mm.)</td>
			<td>700</td>
			<td>700</td>
			<td>700</td>
			<td>700</td>
		</tr>
		<tr>
			<td>Teeth (nr.)</td>
			<td>9</td>
			<td>10</td>
			<td>11</td>
			<td>11</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>