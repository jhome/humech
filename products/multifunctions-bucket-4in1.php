<!-- type in the product title and url -->
<?php $productUrl = "multifunctions-bucket-4in1"; ?>
<?php $productTitle = "Multifunctions Bucket 4 in 1"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Multifunction bucket 4 in 1, useful for every digging works: charge, discharge, levelling and catching.</p>
<p>It can be completed, on request, by bolted teeth with removable protection, and by a bolted and reversible underblade avoiding bucket’s wear.</p>
<p>It is standard equipped with universal linkage for skid-steer loaders, double effect cylinders, blocking valve, rubber hoses and quick couplers for hydraulic circuit supply.</p>
<ul>
	<li>Bolted and reversible underblade</li>
	<li>Set of bolted teeth and cover</li>
	<li>Quick couplers kit 3/4 S.F.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>B4-1400</th>
			<th>B4-1550</th>
			<th>B4-1730</th>
			<th>B4-1880</th>
			<th>B4-2030</th>
			<th>B4-2300</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1400</td>
			<td>1550</td>
			<td>1730</td>
			<td>1880</td>
			<td>2030</td>
			<td> 2300</td>
		</tr>
		<tr>
			<td>Capacity (m³)</td>
			<td>0,330</td>
			<td>0,367</td>
			<td>0,412</td>
			<td>0,449</td>
			<td>0,599</td>
			<td> 0,680</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>330</td>
			<td>350</td>
			<td>400</td>
			<td>440</td>
			<td>510</td>
			<td> 560</td>
		</tr>
		<tr>
			<td>Teeth (nr.)</td>
			<td>6</td>
			<td>6</td>
			<td>7</td>
			<td>7</td>
			<td>8</td>
			<td> 9</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>