<?php $productUrl = "bidirectional-mulching-head"; ?>
<?php $productTitle = "Bidirectional Mulching Head"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">
<?php $img = array('0', '1'); ?>


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<!-- No iamges for this product -->
<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		if (isset($img)){
			foreach($img as $img) {
				if ($img++ == 1) break;
				echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
			}
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Bidirectional mulching head with three double cut blades “Y” shape knives complete by linkage, hydraulic motor with direct transmission, drainage is not necessary. </p>
<p>Our bidirectional mulching head is ideal for mini or midi-excavators, backhoes and backhoe loaders' arms applications. Each application is realised by equipping the brush head with a different size of hydraulic motor to increase the rotation speed.</p>


<ul>
	<li>Quick couplers kit.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>TT 60</th>
			<th>TT 65</th>
			<th>TT 80</th>
			<th>TT 100</th>
			<th>TT 130</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Working width (mm.)</td>
			<td>640</td>
			<td>650</td>
			<td>800</td>
			<td>100</td>
			<td>1300</td>
		</tr>
		<tr>
			<td>Total width (mm.)</td>
			<td>750</td>
			<td>760</td>
			<td>950</td>
			<td>1150</td>
			<td>1700</td>
		</tr>
		<tr>
			<td>Weight (Kg.)</td>
			<td>90</td>
			<td>160</td>
			<td>210</td>
			<td>280</td>
			<td>470</td>
		</tr>
		<tr>
			<td>Maximum oil flow (l/1')</td>
			<td>20-25-35</td>
			<td>25-35-45</td>
			<td>35-45-55</td>
			<td>35-45-55</td>
			<td>65-80</td>
		</tr>
		<tr>
			<td>Maximum pressure (bar)</td>
			<td>250</td>
			<td>250</td>
			<td>250</td>
			<td>250</td>
			<td>250</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>