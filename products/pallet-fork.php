<?php $productUrl = "pallet-fork"; ?>
<?php $productTitle = "Pallet Fork"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<!-- type in the product title and url -->
<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>FPP Model:</p>
<p>FEM (rules for the design of lifting devices) pallet fork with anti-overturning protection, two translatable forks and universal linkage kit for Skid-Steer Loaders.</p>

<p>FP Model:</p>
<p>FEM (rules for the design of lifting devices) pallet fork without anti-overturning protection, two translatable forks and linkage kit for Wheel, Backhoe Loaders and Telehandlers.</p>
<ul>
	<li>Bale Forks</li>
	<li>Anti-overturning protection for FP models</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>FPP10-12</th>
			<th>FPP10-14</th>
			<th>FPP16-15</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Fork (AxBxCxE mm.)</td>
			<td>80x30x900x407</td>
			<td>80x30x1100x407</td>
			<td>100x35x1100x407</td>
		</tr>
		<tr>
			<td>Width (D mm.)</td>
			<td>1210</td>
			<td>1410</td>
			<td>1510</td>
		</tr>
		<tr>
			<td>Capacity (kg.)</td>
			<td>1000</td>
			<td>1000</td>
			<td>1600</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>135</td>
			<td>160</td>
			<td>210</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>