<?php $productUrl = "front-shredder-scorpion"; ?>
<?php $productTitle = "Front Shredder Scorpion"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Front shredder with hammers and mechanical translation, floating linkage plate for Skid-Steer Loader, hydraulic drainage motor and rubber hoses with quick couplers kit for hydraulic circuit supply.</p>
<ul>
	<li>Hydraulic translation complete with electric valve, hydraulic cylinders and electrical kit</li>
	<li>Connector with 8 or 14 poles</li>
	<li>Quick couplers kit 3/4 S.F</li>
	<li>Kit no drainage: pressure and oil flow regulation valve</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>SCORPION 1600</th>
			<th>SCORPION 1900</th>
			<th>SCORPION 2100</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Working width (mm.)</td>
			<td>1600</td>
			<td>1900</td>
			<td>2100</td>
		</tr>
		<tr>
			<td>Total width (mm.)</td>
			<td>1900</td>
			<td>2100</td>
			<td>2300</td>
		</tr>
		<tr>
			<td>Weight (Kg.)</td>
			<td>460</td>
			<td>510</td>
			<td>560</td>
		</tr>
		<tr>
			<td>Maximum oil flow (l/1')</td>
			<td>65-80</td>
			<td>65-80</td>
			<td>65-80</td>
		</tr>
		<tr>
			<td>Maximum pressure (bar)</td>
			<td>200</td>
			<td>200</td>
			<td>200</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>