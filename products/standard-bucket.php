<?php $productUrl = "standard-bucket"; ?>
<?php $productTitle = "Standard Bucket"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">

<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4','5','6'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Standard bucket complete with universal linkage. It can be equipped with a bolted and reversible underblade, as well as teeth complete with removable protection.</p>
<ul>
	<li>Bolted and reversible underblade</li>
	<li>Set of bolted teeth and cover</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>B-STD 1250</th>
			<th>B-STD 1400</th>
			<th>B-STD 1550</th>
			<th>B-STD 1680</th>
			<th>B-STD 1730</th>
			<th>B-STD 1880</th>
			<th>B-STD 2030</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1250</td>
			<td>1400</td>
			<td>1550</td>
			<td>1680</td>
			<td>1730</td>
			<td>1880</td>
			<td>2030</td>
		</tr>
		<tr>
			<td>Capacity (m³)</td>
			<td>0,351</td>
			<td>0,397</td>
			<td>0,442</td>
			<td>0,482</td>
			<td>0,497</td>
			<td>0,543</td>
			<td>0,588</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>230</td>
			<td>240</td>
			<td>250</td>
			<td>260</td>
			<td>270</td>
			<td>280</td>
			<td>310</td>
		</tr>
		<tr>
			<td>Teeth (nr.)</td>
			<td>5</td>
			<td>6</td>
			<td>6</td>
			<td>7</td>
			<td>7</td>
			<td>7</td>
			<td>8</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>