<?php $productUrl = "mixer-bucket-condor"; ?>
<?php $productTitle = "Mixer Bucket Condor"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">
	
<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2','3','4'); ?>

<!-- product title -->
<h2 class="product-title">
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Concrete mixer bucket equipped with an electronic control valve, a button for hydraulic unloading plate opening, unloading opening cylinder with protected rod, bolted protection grate, rubber pipes for hydraulic circuit supply and a linkage kit for Skid-Steer Loader.</p>
<ul>
	<li>Pressure regulation valve</li>
	<li>Hydraulic rotational shaft</li>
	<li>High pressure and high oil flow motor</li>
	<li>Gas spring complete by hydraulic block of rotation’ shaft</li>
	<li>Bolted plate for interchangeable attachments</li>
	<li>Bolted plate for excavators</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Connector with 8 or 14 poles</li>
	<li>Moldboard and screw shaft in Hardox metal</li>
	<li>Reel</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>CONDOR 150</th>
			<th>CONDOR 200</th>
			<th>CONDOR 250</th>
			<th>CONDOR 300</th>
			<th>CONDOR 350</th>
			<th>CONDOR 450</th>
			<th>CONDOR 600</th>
			<th>CONDOR 750</th>
			<th>CONDOR 1000</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Screw shaft diameter (mm.)</td>
			<td>400</td>
			<td>400</td>
			<td>480</td>
			<td> 480</td>
			<td>480</td>
			<td>540</td>
			<td>620</td>
			<td> 620</td>
			<td> 620</td>
		</tr>
		<tr>
			<td>Total bucket width (mm.)</td>
			<td>1170</td>
			<td>1490</td>
			<td>1490</td>
			<td>1575</td>
			<td>1700</td>
			<td>1760</td>
			<td>1760</td>
			<td>2070</td>
			<td>2370</td>
		</tr>
		<tr>
			<td>Working bucket width (mm.)</td>
			<td>940</td>
			<td>1260</td>
			<td>1260</td>
			<td>1345</td>
			<td> 1590</td>
			<td>1590</td>
			<td>1590</td>
			<td>1900</td>
			<td>2200</td>
		</tr>
		<tr>
			<td>Empty weight (kg.)</td>
			<td>255</td>
			<td>290</td>
			<td>345</td>
			<td>365</td>
			<td>420</td>
			<td>590</td>
			<td>680</td>
			<td>760</td>
			<td>920</td>
		</tr>
		<tr>
			<td>Total weight at maximum capacity (kg.)</td>
			<td>710</td>
			<td>900</td>
			<td>1060</td>
			<td>1140</td>
			<td>1330</td>
			<td>2020</td>
			<td>2500</td>
			<td>2930</td>
			<td>3840</td>
		</tr>
		<tr>
			<td>Maximum capacity (lt.)</td>
			<td>190</td>
			<td>255</td>
			<td>300</td>
			<td>325</td>
			<td>380</td>
			<td>600</td>
			<td>760</td>
			<td>910</td>
			<td>1210</td>
		</tr>
		<tr>
			<td>Working capacity (lt.)</td>
			<td>160</td>
			<td>215</td>
			<td>260</td>
			<td>305</td>
			<td>360</td>
			<td>460</td>
			<td>600</td>
			<td>750</td>
			<td>1080</td>
		</tr>
		<tr>
			<td>Maximum oil flow (l/1')</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>90</td>
			<td>100</td>
			<td>100</td>
			<td>135</td>
			<td>220</td>
			<td>220</td>
		</tr>
		<tr>
			<td>Maximum pressure (bar)</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>215</td>
			<td>245</td>
			<td>245</td>
			<td>200</td>
			<td>240</td>
			<td>240</td>
		</tr>
		<tr>
			<td>Unloading hole diameter (mm.)</td>
			<td>130</td>
			<td>180</td>
			<td>180</td>
			<td>180</td>
			<td>180</td>
			<td>180</td>
			<td>180</td>
			<td>180</td>
			<td>180</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>
