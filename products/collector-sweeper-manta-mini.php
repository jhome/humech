<?php $productUrl = "collector-sweeper-manta-mini"; ?>
<?php $productTitle = "Collector Sweeper Manta Mini"; ?>
<?php if(isset($productTitle)) $pageTitle = $productTitle ?>

<?php
include '../inc/head.php'; 
include '../inc/navbar.php';
include '../inc/breadcrumb.php';?>
<article class="page-product">
<aside>
<?php include '../inc/productlist.php';?>
</aside>

<section class="product">


<!-- numbers for each image, so if there are 3 images type 1,2,3, this would output productname1.jpg, productname2.jpg, productname3.jpg and if there is just one type 1 -->
<?php $img = array('0','1','2'); ?>

<!-- product title -->
<h2>
	<?php
		echo $productTitle;
	?>
</h2>
<!-- images  -->
<ul class="img-list">
	<?php
		foreach($img as $img) {
			if ($img++ == 1) break;
			echo  '<li><img src="' . 'img/' . $productUrl . $img .'.jpg"></li>';
		}
	?>
</ul>

<!-- pdf  -->
<div class="info">
	<a href="<?php echo 'pdf/' . $productUrl . '.pdf'?>">PDF 
	</a>
	<a href="<?php echo $baseURL; ?>contact"> Enquire</a>
</div>

<!-- html from here now! -->

<!-- description  + accessories -->
<p>Collector sweeper sized specifically for small machines such as ultra compact skid steer loaders, small articulated loaders, mini excavators etc. Standard brushes are polypropylene with front and side protection by rubber strips. Flexible pipes and quick couplers for hydraulic supply. Universal linkage kit and bolted blades.</p>
<ul>
	<li>Gravity water kit with 50 litres tank.</li>
	<li>Pressure water kit with 50 litres tank, electric pump and sprinklers.</li>
	<li>Pressure water kit with tank fitted on machinery</li>
	<li>Connector with 8 or 14 poles</li>
	<li>Quick couplers kit 3/4 S.F.</li>
	<li>Pressure regulator valve</li>
	<li>Excavator bolted plate</li>
	<li>Double linkage kit: excavator bolted plate and SSL universal linkage.</li>
</ul>

<!-- table -->
<p class="muted"> Please scroll from left to right to view all models. </p>
<div class="responsive">
<table>
	<thead>
		<tr>
			<th>MODELS</th>
			<th>Manta 1100 Mini</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Width (mm.)</td>
			<td>1250</td>
		</tr>
		<tr>
			<td>Workin width (mm.)</td>
			<td>1100</td>
		</tr>
		<tr>
			<td>Weight (kg.)</td>
			<td>170</td>
		</tr>
		<tr>
			<td>Brush (mm/nr.)</td>
			<td>420/29</td>
		</tr>
		<tr>
			<td>Bucket capacity (lit.)</td>
			<td>140</td>
		</tr>
		<tr>
			<td>Maximum oil flow (l/1')</td>
			<td>40</td>
		</tr>
		<tr>
			<td>Maximum pressure (bar.)</td>
			<td>160</td>
		</tr>
	</tbody>
</table>
</div>
</section>
</article>
<?php include '../inc/footer.php'; ?>