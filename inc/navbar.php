    <header> 
    <div class="nav-header">
    <button id="toggle" type="button" class="closed">
     <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
        <a href="<?php echo $baseURL; ?>">
            <img src="<?php echo $baseURL; ?>/img/logo.gif" alt="humech attachments">
        </a>
<!--<a href="./" class="header-name"> Humech </a>-->
</div>
    <nav class="nav-collapse">
      <ul>
        <li><a href="<?php echo $baseURL; ?>">Home</a></li>
        <li><a href="<?php echo $baseURL; ?>products">Products</a></li>
        <li><a href="<?php echo $baseURL; ?>aboutus">About Us</a></li>
        <li><a href="<?php echo $baseURL; ?>contact">Contact</a></li>
      </ul>
    </nav>
</header>

    <script src="<?php echo $baseURL; ?>js/misc.js"></script>
