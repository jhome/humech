<?php
$bucket = array(
  'Mixer Bucket Condor' => 'mixer-bucket-condor',
  'Standard Bucket' => 'standard-bucket',
  'Multifunctional Bucket 4 in 1'=> 'multifunctional-bucket-4in1',
  'Crusher Bucket' => 'crusher-bucket',
);
$fork  = array(
  "Agricultural Fork"=> "agricultural-fork",
  "Pallet Fork" => "pallet-fork",
  "Bale Fork" => "bale-fork",
  "Agricultural Grapple Fork" => "agricultural-grapple-fork",
  "Industrial Grapple Fork" => "industrial-grapple-fork",
);
$grader  = array(
  "Grader Cobra STD" => "grader-cobra-std",
  "Grader Cobra HD" => "grader-cobra-hd",
);
$blade  = array(
  "Angledozer Blade" => "angledozer-blade",
  "Self-Levelling Snowblade" => "snowblade",
  "Tilt Dozer Blade" => "tilt-dozer-blade",
);

$backhoe  = array(
  "Agricultural Backhoe" =>"agricultural-backhoe", 
  "Industrial Backhoe" =>"industrial-backhoe",
);

$sweeper  = array(
  "Collector Sweeper Manta Mini" =>"collector-sweeper-manta-mini",
  "Collector Sweeper Manta STD" =>"collector-sweeper-manta-std",
  "Collector Sweeper Manta HD" =>"collector-sweeper-manta-hd", 
  "Collector Sweeper Manta I" =>"collector-sweeper-manta-i",
  "Angle Broom Pantera" =>"angle-broom-pantera",
  "Solar Panels Broom Manta Solar" =>"solar-panels-broom-manta-solar",
);

$cutter  = array(
  "Bidirectional Mulching Head" =>"bidirectional-mulching-head",
  "Front Shredder Scorpion" =>"front-shredder-scorpion",
);

$blower  = array(
  "Snow Blower HD" =>"snow-blower-yeti-hd",
  "Snow Blower STD" =>"snow-blower-yeti-std",
);

$other = array(
  "Tilt Tatch" =>"tilt-tatch",
  "Auger" =>"auger",
);



/*
$fork  = array("Agricultural Fork", "Pallet Fork", "Bale Fork", "Agricultural Grapple Fork", "Industrial Grapple Fork");
$grader  = array("Grader Cobra STD", "Grader Cobra HD");
$blade  = array("Angle Dozer Blade LA", "Self-Levelling Snowblade", "Tilt Dozer Blade");
$backhoe  = array("Agricultural Backhoe E", "Industrial Backhoe E");
$sweeper  = array("Collector Sweeper Manta Mini", "Collector Sweeper Manta STD", "Collector Sweeper Manta HD", "Collector Sweeper Manta I", "Angle Broom Pantera", "Solar Panels Broom Manta Solar");
$cutter  = array("Bidirectional Mulching Head TT", "Front Shredder Scorpion");
$blower  = arwray("Snow Blower HD", "Snow Blower STD");
$other = array("Tilt Tatch", "Auger");
$bucket = array("Mixer Bucket Condor", "Standard Bucket", "Multifunctional Bucket", "Mixer Bucket Condor", "Crusher Bucket", "Bucket");
$fork  = array("Agricultural Fork", "Pallet Fork", "Bale Fork", "Agricultural Grapple Fork", "Industrial Grapple Fork");
$grader  = array("Grader Cobra STD", "Grader Cobra HD");
$blade  = array("Angle Dozer Blade LA", "Self-Levelling Snowblade", "Tilt Dozer Blade");
$backhoe  = array("Agricultural Backhoe E", "Industrial Backhoe E");
$sweeper  = array("Collector Sweeper Manta Mini", "Collector Sweeper Manta STD", "Collector Sweeper Manta HD", "Collector Sweeper Manta I", "Angle Broom Pantera", "Solar Panels Broom Manta Solar");
$cutter  = array("Bidirectional Mulching Head TT", "Front Shredder Scorpion");
$blower  = array("Snow Blower HD", "Snow Blower STD");
$other = array("Tilt Tatch", "Auger");
*/
?>



<?php if($pageTitle == "Products") {
  $productPageUrl = "./products/"; 
  $productUrl = "0";
}
else {
    $productPageUrl = "./";  
}
?>
    <script src="<?php echo $baseURL; ?>js/foundation.js"></script>
      <script src="<?php echo $baseURL; ?>js/foundation.section.js"></script>


  <ul class="product-list" data-section="accordion">
  <section class="<?php if(in_array($productUrl, $bucket)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Buckets</a></h3>
    <div class="content" data-section-content>
        <?php
        $productState = "active";
    foreach($bucket as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li' . ' class='. $productState . '>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
  <section class="<?php if(in_array($productUrl, $fork)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Forks</a></h3>
    <div class="content" data-section-content>
        <?php
    foreach($fork as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
  <section class="<?php if(in_array($productUrl, $grader)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Graders</a></h3>
    <div class="content" data-section-content>
        <?php
    foreach($grader as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
    <section class="<?php if(in_array($productUrl, $blade)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Blade</a></h3>
    <div class="content" data-section-content>
        <?php
    foreach($blade as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
  <section class="<?php if(in_array($productUrl, $backhoe)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Backhoe</a></h3>
    <div class="content" data-section-content>
        <?php
    foreach($backhoe as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
  <section class="<?php if(in_array($productUrl, $sweeper)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Sweepers</a></h3>
    <div class="content" data-section-content>
        <?php
    foreach($sweeper as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
  <section class="<?php if(in_array($productUrl, $cutter)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Cutters</a></h3>
    <div class="content" data-section-content>
        <?php
    foreach($cutter as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
  <section class="<?php if(in_array($productUrl, $blower)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Blowers</a></h3>
    <div class="content" data-section-content>
        <?php
    foreach($blower as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
  <section class="<?php if(in_array($productUrl, $other)) { echo "active" ; } else {echo "inactive";}?>">
    <h3 class="title" data-section-title> <a href="#">Others</a></h3>
    <div class="content" data-section-content>
        <?php
    foreach($other as $name => $url)
    {
      echo  '<a href="'.$productPageUrl .$url. '">'.  '<li>' . $name . '</li></a>'; 
    }
  ?>
    </div>
  </section>
</ul>
<script>$(document).foundation({
  accordion: {
  deep_linking: false, // Allows for querystring hashtag direct linking
  one_up: true, // Ensures the first section is shown on initial view
  multi_expand: false, // For accordion, allows more than one section to be expanded at the same time.
  rtl: false, // Flags for right-to-left behavior
  callback: function (){} // Called when a section is selected.
  }
});</script>