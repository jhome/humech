<footer> 
  <div class="inner-footer">
  <div class="icon-set-footer">
    <ul class="icon-set">
      <li><a href="tel:01765522010"><i class="el-icon-phone-alt"></i><p class="phone">01765 522 010</p></a></li>
      <li><a href="./contact"><i class="el-icon-envelope-alt"></i><p class="email">contact@humech.co.uk</p></a></li>
    </ul>
  </div>
    <div class="address-footer">
    <address>
      <strong>Registered Address</strong><br>
      Oaktree House<br>
      Aldfield<br>
      Ripon<br>
      North Yorkshire<br>
      HG4 3BE<br><br>
      T: 01765 522 010
    </address>
  </div>
</div>
	<div class="copyright">
		<small>© 2013 Home Corporate Services Ltd - All Rights Reserved. Trading as HUMECH. VAT Number: <em>GB-686987347</em></small>
    <a href="./privacypolicy.pdf" target="_blank">Privacy Policy</a>
  </div>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42245683-1', 'humech.co.uk');
  ga('send', 'pageview');

</script>
</body>
</html>