<?php //$baseURL = "../" ?>
<?php if(isset($productTitle))  $baseURL = '../'; else $baseURL = './';?>

<!doctype html>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="attachments, skid steer, attachments, construction">
		<meta name="description" content="Humech provides a variety of attachments for skid steers, tractors and telehandlers with a universal connection kit.">
		<link rel="canonical" href="http://wwww.humech.co.uk/">
		<title>Humech Attachments <?php echo  $pageTitle ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>css/style.css" /> 
		<script src="<?php echo $baseURL; ?>js/responsive-nav.js"></script>
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"> </script>
		<script src="<?php echo $baseURL; ?>js/misc.js"></script>
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	</head>

<body>

