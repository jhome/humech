<nav class="breadcrumb">
	<a href="<?php echo $baseURL; ?>" class="active">Home</a>
	<span class="divider">&rsaquo;</span>
	<?php if(isset($productTitle)) 
	echo '<a href="' . $baseURL . 'products.php" class="active">Products</a> <span class="divider">&rsaquo;	</span> <a class="inactive">'. $productTitle. '</a>' ;
	else {
		echo '<a class="inactive">'. $pageTitle .'</a>';
	}?>
</nav>