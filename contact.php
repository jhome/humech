<?php 
$emailbox = "contact@codegeeks.co.uk";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = trim($_POST["name"]);
$company =  trim($_POST["company"]);
$email =  trim($_POST["email"]);
$email =  trim($_POST["email"]);
$phone =  trim($_POST["phone"]);
$postcode =  trim($_POST["postcode"]);
$machine =  trim($_POST["machine"]);
$product = trim($_POST['product']);
$message =  trim($_POST["message"]);
	
	
    if ($name == "" OR $email == "" OR $message == "") {
        $errorMessage = "You must fill out the fields: e-mail, message and name.";
    }

    if (!isset($errorMessage)) {
        foreach( $_POST as $value ){
            if( stripos($value,'Content-Type:') !== FALSE ){
                $errorMessage = "There was a problem with the information you entered.";
            }
        }
    }

    if (!isset($errorMessage) && $_POST["address"] != "") {
        $errorMessage = "Your form submission has an error.";
    }

    require_once("inc/class.phpmailer.php");
    $mail = new PHPMailer();

    if (!isset($errorMessage) && !$mail->ValidateAddress($email)){
        $errorMessage = "You must specify a valid email address.";
    }

    if (!isset($errorMessage)) {
        $emailBody = "";
$emailBody = $emailBody . "Name: " . $name ."<br>";
$emailBody = $emailBody .  "Company: ". $company ."<br>";
$emailBody = $emailBody .  "Email: ". $email ."<br>";
$emailBody = $emailBody .  "phone: ". $phone ."<br>";
$emailBody = $emailBody .  "Post code: " . $postcode ."<br>";
$emailBody = $emailBody .  "Machine: " . $machine . "<br>";
$emailBody = $emailBody .  "Product interested in: " . $product . "<br>";
$emailBody = $emailBody .  "Message: ". $message;

        $mail->SetFrom($emailbox, $name);
        $address = "contact@humech.co.uk";
        $mail->AddAddress($address, "humech.co.uk");
        $mail->Subject    = "Form Submission on humech.co.uk | " . $name;
        $mail->MsgHTML($emailBody); 

        if($mail->Send()) {
            header("Location: contact.php?status=thanks");
            exit;
        } else {
          $errorMessage = "There was a problem sending the email: " . $mail->ErrorInfo;
        }

    }
}

?>
<?php 
$pageTitle = 'Contact'; 
$section = "contact";
include 'inc/head.php' ?>
<?php include 'inc/navbar.php' ?>
<?php include 'inc/breadcrumb.php' ?>
<article class="page-contact">
<?php if (isset($errorMessage)) {
echo '<div class="alert alert-error">  <button type="button" class="close" data-dismiss="alert">&times;</button> <h4> Warning! </h4>
' . $errorMessage .'</div>';
}
?>
<?php if (isset($_GET["status"]) AND $_GET["status"] == "thanks" ) { ?>
<p> We will be in contact with you shortly. </p>
</article>
<?php } else { ?>
<p class="muted"> If you have any enquiries or questions please refer to the contact methods below. <br>Please also state your machine model and make in the comment text box.</p>
<div class="content">
<div id="contact">
<form method="post" action="contact.php">
<fieldset>
							  <div class="control-group">
							    <div class="controls">
							      <input type="text" name="name" placeholder="Name" id="name" value="<?php if(isset($name)) { echo htmlspecialchars($name); }?>">
							    </div>
							  </div>
							    <div class="control-group">
							    <div class="controls">
							      <input type="text" name="company" placeholder="Company" id="company" value="<?php if(isset($company)) { echo htmlspecialchars($company); }?>">
							    </div>
							  </div>
							  <div class="control-group">
							    <div class="controls">
							      <input type="text" name="email"placeholder="E-mail Address" id="email" value="<?php if(isset($email)) { echo htmlspecialchars($email); }?>">
							    </div>
							  </div>
							  <div class="control-group">
								   <div class="controls">
								    <input type="text" name="postcode"  placeholder="UK Post Code"id="postcode" value="<?php if(isset($postcode)) { echo htmlspecialchars($postcode); }?>">
								    </div>
							  </div>
							  <div class="control-group">
								    <div class="controls">
								      <input type="text" name="phone"  placeholder="Telephone Number"id="phone" value="<?php if(isset($phone)) { echo htmlspecialchars($phone); }?>">
								    </div>
							  </div>
							  	<div class="control-group">
								    <div class="controls">
								      <select name="product" data-label="Type of Attachment" id="product" value="<?php if(isset($product)) { echo htmlspecialchars($product); }?>">
										<option>What product are you most interested in?</option>
										<option>Bucket</option>
										<option>Fork</option>
										<option>Grader</option>
										<option>Blade</option>
										<option>Backhoe</option>
										<option>Sweeper</option>
										<option>Cutter</option>
										<option>Blower</option>
										<option>Tilt Tatch</option>
										<option>Auger</option>

								      </select>
								    </div>
							  </div>
							  <div class="control-group">
								    <div class="controls">
								      <input type="text" name="machine" list="machine" placeholder="Please enter your machine" id="machine" value="<?php if(isset($machine)) { echo htmlspecialchars($machine); }?>">
								    </div>
							  </div>
								<div class="control-group">
								    <div class="controls">
								      <textarea type="text" name="message" rows="6"  placeholder="Please include as much detail as possible." id="message"><?php if(isset($message)) { echo htmlspecialchars($message); }?></textarea>
								    </div>
							  </div>
							  <div class="control-group" style="display: none;">
							    <div class="controls">
								<label for ="address"> Address </label>
							      <input type="text" name="address" id="address">
								  <p> This field is an anti-spam measure against bots filling out our form. Please do not enter any details on into this field as the form will be rejected.</p>
							    </div>
							  </div>
							  	 <div class="control-group">
							    <div class="controls">
							      <button type="submit" value="Send" class="btn btn-primary">Submit</button>
							    </div>
							  </div>
							</fieldset>
						</form>
</div>
	<div id="contact_address">
<address>
  <strong>Registered Address</strong><br>
 Oaktree House<br>
      Aldfield<br>
      Ripon<br>
      North Yorkshire<br>
      HG4 3BE<br><br>
   <abbr title="Tel">T:</abbr> 01765 522 010
</address>
</article>
<?php } ?>
<script src="js/jquery.placeholder-enhanced.js"></script>
<script src="js/alert.bootstrap.min.js"></script>

<?php include 'inc/footer.php'; ?>

