<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Maintenance</title>
</head>
<style>
	*, *:before, *:after {
	  -moz-box-sizing: border-box;
	  -webkit-box-sizing: border-box;
	  box-sizing: border-box;
	}
	body {
		font-family: Helvetica, Sans-Serif;
		margin: 0;
		padding: 0;
		border: 0;
	}
	a {
		text-decoration: none;
		color: #e1451f;
	}
	a:hover {
		color: black;
	}
	.notice {
		margin: 5% 5% 10% 10%;
		text-align: center;
	}
</style>
<body>
	<div class="notice">
		<h1>Our site is currently under maintenance.</h1>
		<p>This should only take up to 24 hours. Thank you for your visit.</p>
		<p>For contact and information about our products please email us below:</p>
		<a href="mailto:contact@humech.co.uk">contact@humech.co.uk</a>
	</div>
</body>
</html>