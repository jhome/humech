<?php
$pageTitle = "Page not found"; 

include '../inc/head.php';

?>	


<article class="four-zero-four">
	<section class="top-four-zero-four">
		<h1>404: Page Not Found</h1>
		<p>We are sorry but we could not locate the page you entered. Please try the following links:</p>
	</section>
	<section class="bottom-four-zero-four">
		<div class="link-group">
			<ul class="links-main">
				<li>
					<a href="/">
						Main Page
					</a>
				</li>
				<li>
					<a href="../aboutus">
						About Us
					</a>
				</li>
				<li>
					<a href="../contact">
						Contact Us
					</a>
				</li>
				<li>
					<a href="../products">
					Products
					</a>
				</li>
			</ul>
		</div>
	</section>
</article>
