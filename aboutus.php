<?php
$pageTitle = "About Us"; 

include './inc/head.php';
include './inc/navbar.php';
include './inc/breadcrumb.php';
?>

<article class="page-aboutus">
	<section class="page-aboutus">
			<p>HUMECH is a UK provider of high quality earth moving equipment for construction, landscaping and agriculture. We supply a wide range of machinery from industrial and agricultural forks to mixer buckets. We invite you to look through our range and contact us with any questions you may have, but if you are unable to find the attachment you are looking for please contact us at <a href="mailto:sales@humech.co.uk">sales@humech.co.uk</a> as we are advancing our products continuously, and not all our products are shown on the website.  </p> 
<p> We however do not offer second hand products. </p>
	<p>Home Corporate Services Limited has been operating since 1996 and trades as HUMECH.</p>
	</section>
	<img class="aboutus-img" src="<?php echo $baseURL; ?>img/collage1.jpg" alt="Variety of attachments">
</article>

<?php include './inc/footer.php'; ?>