<?php
$pageTitle = "Products"; 

include './inc/head.php'; 
include './inc/navbar.php';
include './inc/breadcrumb.php';
?>

<article class= "page-products">
	<aside>
	<?php include './inc/productlist.php'; ?>
	</aside>
	<section class="page-products">
			<div class="video right" id="video">
		<video width="100%" controls poster="<?php echo $baseURL; ?>img/video-poster.jpg">
			<source src="<?php echo $baseURL; ?>vid/4in1.ogv" type="video/ogg" type='application/ogg'/>
			<source src="<?php echo $baseURL; ?>vid/4in1.mp4" type="video/mp4" type='video/mp4' />
			<object width="640" height="360" type="application/x-shockwave-flash" data="<?php echo $baseURL; ?>vid/4in1.SWF">
			<param name="movie" value="<?php echo $baseURL; ?>vid/4in1.swf" />
			<param name="flashvars" value="controlbar=over&amp;image=__POSTER__.JPG&amp;file=<?php echo $baseURL; ?>vid/4in1.MP4" />
			<img src="<?php echo $baseURL; ?>/products/img/multifunctional-bucket-4in11.jpg" width="640" height="360" alt="__TITLE__"
		     title="No video playback capabilities, please download the video below" />
			</object>
		</video>

		<div class="download-list">
			<a href="<?php echo $baseURL; ?>products/multifunctional-bucket-4in1">Click Here to View: Multifunctional Bucket 4 in 1</a>
		</div>
	</div>
		<p>HUMECH sells a complete range of equipment for mini, midi or larger excavators, skid-steer loaders, wheel loaders, backhoe loaders, agricultural tractors and telehandlers for the earthmoving machinery sector. Select the category which you are interested from the menu on the left and discover our products:</p>
		<ul>
			<li>Standard buckets</li>
			<li>Multifunctional buckets</li>
			<li>Concrete mixer buckets</li>
			<li>Agricultural and industrial grapple forks</li>
			<li>Road graders</li>
			<li>Snow blades and refill blades</li>
			<li>Sweepers</li>
			<li>Angle brooms and solar panel brooms (Manta Solar)</li>
			<li>Agricultural and industrial backhoes</li>
			<li>Front shredders and bush cutters for excavators</li>
			<li>Snow blowers</li>
			<li>Auger</li>
		</ul>
	</section>
</article>


<?php include './inc/footer.php'; ?>